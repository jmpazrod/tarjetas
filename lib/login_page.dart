import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tarjetas/pages/home.dart';


class LoginPage extends StatelessWidget {

  Widget createEmailInput(){
    return Padding(
            padding: const EdgeInsets.only(top:30),
            child: TextFormField(decoration: InputDecoration(hintText: 'Usuario'),
            ),
          );

  }

  Widget createPasswordinput(){
    return Padding(
            padding: const EdgeInsets.only(top: 30),
            child: TextFormField(decoration: InputDecoration(hintText: 'Contraseña'),
            obscureText: true,

            ),
          );
  }

  Widget createLoginButton(BuildContext context){
    return Container(
      padding: const EdgeInsets.only(top: 32),
      child: RaisedButton(child: Text('Entrar'),
      onPressed: (){
        Navigator.of(context).pushNamed('/home');
      },
      ));
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: new AppBar(
            title: new Text ("API Tarjetas")
          ),
          body: Container(
          padding: EdgeInsets.symmetric(horizontal:16),
          decoration: BoxDecoration(color: Colors.white),
          child: ListView(children: <Widget>[
          
          Image.asset('assets/images/tarjetas.png',
          width: 400,
          ),
          createEmailInput(),
          createPasswordinput(),
          createLoginButton(context)
          
        ]),
      ),
    );
  }
}

